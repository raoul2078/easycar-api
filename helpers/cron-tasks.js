const cron = require('node-cron');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');

require('../models/User');
const User = mongoose.model('users');

let task = cron.schedule('* * * * *', function(){
  User.find({}).then(users => {
    users.forEach((user) => {
      user.tokens.forEach((tokenObj) => {
        let decoded;
        let token = tokenObj.token;
        try {
          decoded = jwt.verify(token, 'abc123');
        } catch (e) {
          console.log(e.message);
          user.removeToken(token).then(()=> {console.log('updated')}).catch(e => {console.log(e.message)})
        }
      })
    });
  }).catch(e => {
    console.log(e.message);
  })
},false);

task.start();
// module.exports = {task};