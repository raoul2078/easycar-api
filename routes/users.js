const _ = require('lodash');
const fs = require('fs');
const express = require('express');
const mongoose = require('mongoose');
const router = express.Router();
const bcrypt = require('bcryptjs');
const multer = require('multer');
const path = require("path");

const DIR = 'public/uploads/profile/';
const DEFAULT_PIC = 'default-profile.jpg';
//Set storage engine
const storage = multer.diskStorage({
  destination: DIR,
  filename: function (req, file, cb) {
    cb(null, file.fieldname + '-' + Date.now() + '-' + file.originalname);
  }
});

//Init Upload
const upload = multer({
  storage: storage,
  limits: {fileSize: 10 * 1024 * 1024},
  fileFilter: function (req, file, cb) {
    checkFileType(file, cb);
  }
}).single('photo');

//Check File Type : we only want images
function checkFileType(file, cb) {
  const filetypes = /jpeg|jpg|png|gif/;
  const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
  //Check mime
  const mimetype = filetypes.test(file.mimetype);

  if (mimetype && extname) {
    return cb(null, true);
  } else {
    cb('Error: Images Only');
  }
}

//Authentication middleware
const {authenticate} = require('../helpers/auth');

//Load User Model
require('../models/User');
const User = mongoose.model('users');

//GET Users
router.get('/', (req, res) => {
  User.find().then(users => {
    res.send(users)
  }).catch(e => {
    res.status(400).send(e);
  })
});

//POST User (Register)
router.post('/', (req, res) => {
  let body = _.pick(req.body, ['firstName', 'lastName', 'gender', 'email', 'password', 'role', 'birth', 'address', 'phoneNumber']);
  body.birth = new Date(body.birth);
  let user = new User(body);

  user.save().then(() => {
    return user.generateAuthToken();
  }).then((token) => {
    res.header('x-auth', token).send(user);
  }).catch(e => {
    res.status(400).send(e);
  });
});

router.patch('/activate-deactivate/:id', authenticate, (req, res) => {
  if (req.user.role !== 'admin') {
    return res.status(401).send();
  }
  User.findOneAndUpdate({
      _id: req.params.id
    },
    {$set: {isActive: req.body.isActive}},
    {$new: true}).then(user => {
    if (!user) {
      res.status(404).send()
    }
    return res.send(user);
  }).catch(e => {
    res.status(400).send(e)
  })
});


router.patch('/change-role/:id', authenticate, (req, res) => {
  if (req.user.role !== 'admin') {
    return res.status(401).send();
  }
  User.findOneAndUpdate({
      _id: req.params.id
    },
    {$set: {role: req.body.role}},
    {$new: true}).then(user => {
    if (!user) {
      res.status(404).send()
    }
    return res.send(user);
  }).catch(e => {
    res.status(400).send(e)
  })
});

router.get('/me', authenticate, (req, res) => {
  res.send(req.user);
});

//User login
router.post('/login', (req, res) => {
  let body = _.pick(req.body, ['email', 'password']);

  User.findByCredentials(body.email, body.password).then((user) => {
    return user.generateAuthToken().then((token) => {
      res.header('x-auth', token).send(user);
    });
  }).catch(e => {
    res.status(400).send(e);
  })
});

router.patch('/me', authenticate, (req, res) => {
  let body = _.pick(req.body, ['firstName', 'lastName', 'email', 'address', 'phoneNumber']);

  User.findOneAndUpdate({
      _id: req.user._id
    },
    {$set: body},
    {new: true}).then((user) => {
    if (!user) {
      res.status(404).send();
    }
    res.send(user);
  }).catch(e => {
    res.status(400).send(e);
  });
});

router.patch('/change-password', authenticate, (req, res) => {
  let body = _.pick(req.body, ['current', 'newPassword']);
  let pwd;
  bcrypt.genSalt(10, (err, salt) => {
    bcrypt.hash(body.newPassword, salt, (err, hash) => {
      pwd = hash;
    })
  });

  User.findByCredentials(req.user.email, body.current).then((user) => {
    User.findOneAndUpdate({
        _id: req.user._id
      },
      {$set: {password: pwd}},
      {new: true}).then((user) => {
      if (!user) {
        res.status(404).send();
      }
      res.send(user);
    }).catch(e => {
      res.status(400).send(e);
    });
  }).catch(e => {
    res.status(400).send('Mot de passe incorrect');
  })

});

//User logout
router.delete('/me/token', authenticate, (req, res) => {
  req.user.removeToken(req.token).then(() => {
    res.status(200).send({isDone: true});
  }, () => {
    res.status(400).send();
  });
});

// upload profile picture
router.post('/upload', authenticate, (req, res) => {
  let path = '';
  upload(req, res, (err) => {
    if (err) {
      res.status(422).send(err)
    } else {
      path = req.file.path;
      if (req.user.photo !== DEFAULT_PIC) {
        fs.unlink(DIR + req.user.photo, (err) => {
          if (err) {
            return console.log(err);
          }
        })
      }
      User.findOneAndUpdate({
          _id: req.user._id
        },
        {$set: {photo: path.replace(DIR, '')}},
        {new: true}).then(user => {
        if (!user) {
          res.status.send(404);
        }
        res.send({user});
      }).catch(e => {
        res.status(400).send(e);
      })
    }
  });
});

// delete profile picture
router.delete('/upload', authenticate, (req, res) => {
  if (req.user.photo !== DEFAULT_PIC) {
    fs.unlink(DIR + req.user.photo, (err) => {
      if (err) {
        return console.log(err);
      }
    })
  }
  User.findOneAndUpdate({
      _id: req.user._id
    },
    {$set: {photo: DEFAULT_PIC}},
    {new: true}).then(user => {
    if (!user) {
      res.status.send(404);
    }
    res.send({user});
  }).catch(e => {
    res.status(400).send(e);
  })

});

router.delete('/notification/:id', authenticate, (req, res) => {
  User.findOneAndUpdate({
      _id: req.user._id
    },
    {
      $pull: {notifications: {_id: req.params.id}}
    }).then(user => {
    if (!user) {
      res.status(404).send();
    }
    res.send({user});
  }).catch(e => {
    res.status(400).send(e)
  })
});
module.exports = router;