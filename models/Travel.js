const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TravelSchema = new Schema({
  departure: {
    type: String,
    required: true,
  },
  destination: {
    type: String,
    required: true
  },
  departureAddress: {
    type: String,
    required: true
  },
  destinationAddress: {
    type: String,
    required: true
  },
  price: {
    type: Number,
    required: true
  },
  date: {
    type: Date,
    required: true,
    default: Date.now
  },
  placesAvailable: {
    type: Number,
    required: true
  },
  car: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'cars'
  },
    averageTime: {
        type: Number,
        required: true,
        default: 0
    },
    distance: {
        type: Number,
        required: true,
        default: 0
    },
    isOnline: {
        type: Boolean,
        required: true,
        default: true
    },
  user: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'users'
  },
  passengers: [
    {
      type: Schema.Types.ObjectId,
      ref: 'users'
    }
  ],
  comments: [{
    commentBody: {
      type: String,
      required: true
    },
    rating: {
      type: Number,
      required: true,
      default: 0
    },
    commentDate: {
      type: Date,
      default: Date.now()
    },
    commentUser: {
      type: Schema.Types.ObjectId,
      ref: 'users'
    }
  }
  ]
});

mongoose.model('travels', TravelSchema);

